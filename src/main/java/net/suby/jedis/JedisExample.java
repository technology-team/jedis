package net.suby.jedis;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

/**
 * Created by ohs on 2016-12-27.
 */
public class JedisExample {
    public static void main(String[] args){
        jedisExpireTime();
    }

    public static void jedisSetGet(){
        // redis 연결 설정 및 접속정보(S)
        JedisPoolConfig jedisPoolConfig = new JedisPoolConfig();
        JedisPool pool = new JedisPool(jedisPoolConfig, "192.168.56.129", 6379, 1000);

        Jedis jedis = pool.getResource();
        // redis 연결 설정 및 접속정보(E)
        jedis.set("data","haksup");     // data set
        String value = jedis.get("data");
        System.out.println(value);

        jedis.del("data");
        System.out.println(value);

        if(jedis != null){
            jedis.close();
        }
    }

    public static void jedisExpireTime(){
        // redis 연결 설정 및 접속정보(S)
        JedisPoolConfig jedisPoolConfig = new JedisPoolConfig();
        JedisPool pool = new JedisPool(jedisPoolConfig, "192.168.56.129", 6379, 1000);

        Jedis jedis = pool.getResource();
        // redis 연결 설정 및 접속정보(E)
        try{
            jedis.set("key", "value");
            jedis.expire("key", 5); // 해당 입력하는 값은 초(sec) 단위입니다.
        }catch(Exception e){
        }finally{
            if( jedis != null ){
                jedis.close();
            }
        }
    }
}
