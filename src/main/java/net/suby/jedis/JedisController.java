package net.suby.jedis;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by ohs on 2016-12-27.
 */
@Controller
public class JedisController {
    @RequestMapping("/hello")
    @ResponseBody
    public Map<String, Object> hello(HttpServletRequest request){
        Map<String, Object> jsonObject = new HashMap<String, Object>();
        jsonObject.put("hi", "hello");

        return jsonObject;
    }

    @RequestMapping("/login/{userId}/{sessionId}")
    @ResponseBody
    public Map<String, Object> login(HttpServletRequest request, @PathVariable String userId, @PathVariable String sessionId){
        Map<String, Object> jsonObject = new HashMap<String, Object>();

        if(sessionId == "undefined"){
            // redis에 session 있는지 확인
        } else {    // 세션 생성
            HttpSession httpSession = request.getSession();
            httpSession.setAttribute("sessionId", httpSession.getId());
            jsonObject.put("sessionId", httpSession.getAttribute("sessionId"));
            // redis 저장
        }
        return jsonObject;
    }

    @RequestMapping("/setRedis/{key}/{value}")
    @ResponseBody
    public String setRedis(@PathVariable String key, @PathVariable String value){
        Jedis jedis = jedisFactory();
        String result = jedis.set(key, value);
        System.out.println("result " + result);
        return result;
    }

    public Jedis jedisFactory(){
        // redis 연결 설정 및 접속정보(S)
        JedisPoolConfig jedisPoolConfig = new JedisPoolConfig();
        JedisPool pool = new JedisPool(jedisPoolConfig, "192.168.56.129", 6379, 1000);

        Jedis jedis = pool.getResource();
        // redis 연결 설정 및 접속정보(E)
        return jedis;
    }
}
